import React, { useState,useEffect } from "react";
import {useHistory} from "react-router-dom";
import Avatar from 'react-avatar';

import '../Users.css';

/**
 * Component for Listing all users.
 * @returns html
 */
const UserList = (props) => {
const[error,setError]= useState(null);
const [usersList,setUserList]=useState([]);
const [isLoaded,setIsLoaded] = useState(false);
const history = useHistory();

/**
 * Function for navigating to user info page.
 * @returns html
 */
const handileClick = (user)=>{
    history.push({pathname:'/user-info',user});
}


 useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(res => res.json())
    .then(
        (result) => {
            setUserList(result);
            setIsLoaded(true);
        },
        (error) => {
            setError(error);
        }
    );
 }, []);

 if (error) {
    return <div>Something went wrong! {error.message}</div>;
    } else if (!isLoaded) {
    return <div>Loading...</div>;
    } else {
    return (
        <div className="user-list">
            <h2>Users</h2> <br/>
            <ul>
            {usersList.map(item => (
                <li key={item.id} onClick={()=>{handileClick(item)}}>
                <Avatar name={item.name} size="50" round={true} fgColor={"black"} color={'White'}/> {item.name}  {item.email}
                </li>
            ))}
            </ul>
        </div>

    );
    }
 };

export default UserList;
