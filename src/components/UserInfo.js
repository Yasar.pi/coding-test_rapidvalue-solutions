import React from "react";
import {useHistory} from "react-router-dom";

/**
 * Component for Listing all users.
 * @returns html
 */
const UserInfo = (props) => {
    const { user } = (props.location) || {};
    const history = useHistory();

   /**
   * Function for navigating to user list page.
   * @returns html
   */
    const handileClick = (user)=>{
        history.push('/users');
    }

    if(user)
    {
        return (
            <div className="card">
                <p><span className="title"> Address:</span> <span className="info">{user.address.suite}, {user.address.street}, {user.address.city}, {user.address.zipcode} </span></p>
                <p><span className="title"> Phone:</span> <span className="info">{user.phone}</span></p>
                <p><span className="title"> Website:</span> <span className="info">{user.website}</span></p>
                <p><span className="title"> Company Name:</span> <span className="info">{user.company.name}</span></p>
                <p><button onClick={handileClick}>Back</button></p>
            </div>
        );
    }
    else
    {
     history.push('/users');
     return null;
    }

};

export default UserInfo;