import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import UserList from "./components/UserList";
import UserInfo from "./components/UserInfo";

import './App.css';

function App() {
  return (
    <div className="App">
        <Router>
          <div>
            <Switch>
              <Route
                exact
                path="/"
                render={() => {
                    return (
                      <Redirect to="/users" />
                    )
                }}
              />
               <Route exact path="/users" component={UserList} />
               <Route exact path="/user-info" component={UserInfo} />
            </Switch>
          </div>
        </Router>
    </div>
  );
}

export default App;
